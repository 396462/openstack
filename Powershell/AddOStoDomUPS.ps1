#ps1_sysnative
Start-Transcript -Path "C:\windows\temp\$(get-date -format "yyyy-MM-dd_hh-mm-ss").txt";
$user = [ADSI]'WinNT://./Administrator';
$user.SetPassword('administrator_password');
NET USER PB007 pb007_password /add;
NET LOCALGROUP Administrators PB007 /add;
NET USER admin /delete;
$secpasswd = ConvertTo-SecureString "domain_password" -AsPlainText -Force;
$mycreds = New-Object System.Management.Automation.PSCredential ("UPS\addtodomOPENSTACK", $secpasswd);
Add-Computer -DomainName "ups.ucn.muni.cz" -OUPath "OU=PB007,OU=OPENSTACK,DC=ups,DC=ucn,DC=muni,DC=cz" -Credential $mycreds;
$rawData = Invoke-WebRequest -Uri "https://gitlab.ics.muni.cz/396462/openstack/raw/master/Powershell/PB007-students-list.json" -UseBasicParsing
$data = $rawData | ConvertFrom-Json
$data.computers | %{
    $pcName = $_.hostname
    $userNames = $_.users
    if($_.hostname -eq $env:computername){
        Write-Output "Hostname: $($env:computername)"
        $currentData = net localgroup "Remote Desktop Users" 
        
        $userNames | %{       
            
            if($currentData -like "*ucn\$($_)*"){
            
                "ucn\$($_) already member of group"
            }else{
            
                Write-Output "ucn\$($_)"
                NET LOCALGROUP "Remote Desktop Users" "ucn\$($_)" /add;
            }
        }
    }
}
shutdown -r -t 0 -f
Stop-Transcript;