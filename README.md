# PB007 Openstack template #
- creates and joins VMs to ups.ucn.muni.cz AD domain. 
- assigns RDP access to users based on json definition - https://gitlab.ics.muni.cz/396462/openstack/blob/master/Powershell/PB007-students-list.json


## Manual - Create stack ##
1. Log in to https://ostack.ics.muni.cz
2. Choose PB007 project
3. Go to Orchestration -> Stacks
4. Launch stack 
5. Fill data
    * Template source - URL
    * Template URL - https://gitlab.ics.muni.cz/396462/openstack/raw/master/Templates/JSON/PB007-stack.json
    * Environment Source - Direct Input
    * Environment Data - Copy content of https://gitlab.ics.muni.cz/396462/openstack/blob/master/Templates/JSON/pb007-env.json
    
    ![Launch stack](https://gitlab.ics.muni.cz/396462/openstack/raw/master/Manual/1.PNG)

6. Fill Data
    * Stack Name - PB007-Stack
    * Creation Timeout - 60
    * Rollback On Failure - unchecked
    * Password for user - Secondary password
    * Administrator password - <pw> for local administrator account
    * Domain password - <pw> of user which adds computers to ups.ucn.muni.cz domain
    * Number of servers - <int> number of instances - can be changed later
    * Image ID - <guid> of prepared image
    * PB007 password - <pw> of local user PB007 with administrator rights

    ![Launch stack](https://gitlab.ics.muni.cz/396462/openstack/raw/master/Manual/2.PNG)

7. Check status of created stack

    ![Launch stack](https://gitlab.ics.muni.cz/396462/openstack/raw/master/Manual/3.PNG)
    

## Manual - Change stack configuration ##

1. Log in to https://ostack.ics.muni.cz
2. Choose PB007 project
3. Go to Orchestration -> Stacks
4. Choose stack and click "Change Stack Template"

    ![Launch stack](https://gitlab.ics.muni.cz/396462/openstack/raw/master/Manual/4.PNG)

5. Fill new data
6. Check status of operation

    ![Launch stack](https://gitlab.ics.muni.cz/396462/openstack/raw/master/Manual/5.PNG)

